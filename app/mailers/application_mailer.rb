class ApplicationMailer < ActionMailer::Base
  default from: "PrestoFacto <contact@prestofacto.fr>"
  layout 'mailer'
end
